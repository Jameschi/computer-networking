#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include "ai.h"

void prepare(void) {
    int i, tmp, unit_1, unit_10, unit_100, unit_1000;
    remain = 5040; 
    tmp = 0;
    isOver = 0;
    for(i = 0; i < 10000; i++){
        unit_1 = (i / 1) % 10;
        unit_10 = (i / 10) % 10;
        unit_100 = (i / 100) % 10;
        unit_1000 = (i / 1000) % 10;
        if(unit_1 != unit_10 && unit_1 != unit_100 && unit_1 != unit_1000 && unit_10 != unit_100 && unit_10 != unit_1000 && unit_100 != unit_1000) {
            answerBase[tmp][0] = unit_1000;
            answerBase[tmp][1] = unit_100;
            answerBase[tmp][2] = unit_10;
            answerBase[tmp][3] = unit_1;
            ++tmp;
        }
    }
}
void ask(int* buffer) {
    int i;
    srand(time(0));
    while(1) {
        randNum = 0;
        for(i = 0; i < 4; i++) {
            randNum *= 10;
            randNum += rand() % 10;
        }
        if(randNum < remain) break;
    }
    buffer[0] = answerBase[randNum][0]; 
    buffer[1] = answerBase[randNum][1];
    buffer[2] = answerBase[randNum][2];
    buffer[3] = answerBase[randNum][3];
}
void think(void){
     if (a == 4) {
	isOver = 1;
	return ;
     }
     int i, j, k, aa, bb;
     int refer[4];
     for(i = 0; i < 4; i++) {
         refer[i] = answerBase[randNum][i];
     }
     for(i = 0;i < remain; i++) {
         aa = bb = 0; 
         for(j = 0; j < 4; j++) {
             for(k = 0; k < 4; k++) {
                 if(answerBase[i][j] == refer[k]) {
                     if(j == k) aa++;
                     else bb++; 
                 }
             }
         }
         if(!((aa == a) && (bb == b))) {
             for(j = i;j < remain; j++){
                 for(k = 0; k < 4; k++){
                     answerBase[j][k] = answerBase[j + 1][k];
                 }
             }
             remain--;
             i--;
         }
     }
}
