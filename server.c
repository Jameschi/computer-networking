#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
void det(int s[], int num[], int* a, int* b) {
	if (num[3] == s[3]) ++(*a);
	else if (num[3] == s[2] || num[3] == s[1] || num[3] == s[0])	++(*b);
	if (num[2] == s[2]) ++(*a);
	else if (num[2] == s[3] || num[2] == s[1] || num[2] == s[0])	++(*b);
	if (num[1] == s[1]) ++(*a);
	else if (num[1] == s[3] || num[1] == s[2] || num[1] == s[0])	++(*b);
 	if (num[0] == s[0]) ++(*a);
	else if (num[0] == s[3] || num[0] == s[2] || num[0] == s[1])	++(*b);
}
void swap(int* x, int* y) {
	int temp = *y;
	*y = *x;
	*x = temp;
}
void genNum(int x[]) {
	int array[10];
	int i;
	srand(time(0));
	int randNum;
	for (i = 0; i < 10; ++i)
		array[i] = i;
	for (i = 9; i >= 0; --i) {
		randNum = rand() % (i + 1);
		swap(&array[randNum], &array[i]);
	}
	for (i = 0; i < 4; ++i) {
		x[i] = array[i];
	}
}

void shuffle(int* card) {
	int i;
	for (i = 0; i < 52; ++i) {//fill in the card
		card[i] = i % 13 + 1;
	}
	//shuffle the card
	srand(time(0));
	int randNum;
	for (i = 51; i >= 0; --i) {
		randNum = rand() % (i + 1);
		swap(&card[randNum], &card[i]);
	}
}	
int calculate(const int* const x, int cnt) {
	int i, result = 0, ace = 0;
	for (i = 0; i < cnt; ++i) {
		if (x[i] == 1) {
			++ace;
			result += 11;
		}
		else if (x[i] >= 10)
			result += 10;
		else result += x[i];
	}
	while (ace > 0 && result > 21) {
		--ace;
		result -= 10;
	}
	if (result > 21)
		return -1;
	else
		return result;
}
int main(int argc, char *argv[]) {
	char buffer[50];
	int k = atoi(argv[1]);
	int listenfd, connfd[k];
	socklen_t length;
	struct sockaddr_in serverAddress, clientAddress;
	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	int on = 1;
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on));
	bzero(&serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(5000);
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(listenfd, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
	listen(listenfd, 1);
	int i, cnt = 1;
	char type;
	length = sizeof(clientAddress);
	//game type is determined by first player
	connfd[0] = accept(listenfd, (struct sockaddr*)&clientAddress, &length);
	read(connfd[0], &type, sizeof(char));
	write(connfd[0], &type, sizeof(char));//tell player game type
	for (i = 1; i < k; ++i) {//accept k player into the system
		char tempType;
		connfd[cnt] = accept(listenfd, (struct sockaddr*)&clientAddress, &length);
		read(connfd[cnt], &tempType, sizeof(char));
		if (tempType == type)
			write(connfd[cnt++], &type, sizeof(char));//tell player game type
		else write(connfd[cnt], "R", sizeof(char));//reject
	}
	k = cnt;
	if (type == 'G') {//guess number
		int result[k], readBuf[4];
		//choose a number
		int num[4];
		genNum(num);
		printf("number is %d%d%d%d\n", num[0], num[1], num[2], num[3]);
		for(i = 0; i < k; ++i) {//play game in order
			int j, l;
			for(j = 0; j < 10; ++j) {//guess at most 10 times otherwise lose the game
				int A = 0, B = 0;
				for(l = 0; l < 4; ++l) {
					read(connfd[i], &readBuf[l], sizeof(int));//read the number gussed by player
				}
				det(readBuf, num, &A, &B);//calculate xAyB
				write(connfd[i], &A, sizeof(int));
				write(connfd[i], &B, sizeof(int));
				if (A == 4) {//player i guess the right number in round i
					result[i] = j + 1;
					break;
				}
			}
			//if we arrive at here, means player doesn't guess the right number in 10 rounds
			if (j == 10)	result[i] = -1;
		}
		int min = result[0];
		for (i = 1; i < k; ++i) {
			if (result[i] < min)
				min = result[i];
		}
		for (i = 0; i < k; ++i) {//calculate the result and send data to k players and close connection
			write(connfd[i], &min, sizeof(int));
			write(connfd[i], &result[i], sizeof(int));
			close(connfd[i]);
		}
	}
	else {//blackjack part
		int card[52], hand[21] = {0}, handCnt = 2;
		int allCards[13] = {0};
		char choice = 'A';
		shuffle(card);
		int cur = 0, myCard = 0;
		//deal one card to each player visible
		for (i = 0; i < k; ++i) {
			++allCards[card[cur] - 1];
			write(connfd[i], &card[cur++], sizeof(int));
		}
		//deal to myself invisible
		hand[0]= card[cur++];
		//deal another card to each player visible
		for (i = 0; i < k; ++i) {
			++allCards[card[cur] - 1];
			write(connfd[i], &card[cur++], sizeof(int));
		}
		//deal to myself
		++allCards[card[cur] - 1];
		hand[1] = card[cur++];
		printf("The two cards you get are: %d %d\n", hand[0], hand[1]);
		//start to play the game in order
		for (i = 0; i < k; ++i) {
			int j;
			for (j = 0; j < 13; ++j) {
				write(connfd[i], &allCards[j], sizeof(int));//tell player i all the visible cards until now
			}
			while(choice != 'T') {
				read(connfd[i], &choice, sizeof(char));//parse player i request
				switch (choice) {
					case 'I'://HIT
						++allCards[card[cur] - 1];
						//printf("%d\n", card[cur] - 1);
						write(connfd[i], &card[cur++], sizeof(int));
						break;
					case 'T'://STAND
						break;
					case 'P'://SPLIT
						++allCards[card[cur] - 1];
						write(connfd[i], &card[cur++], sizeof(int));	
						++allCards[card[cur] - 1];
						write(connfd[i], &card[cur++], sizeof(int));
						break;
					default:
						perror("wrong input");
				}
			}
			choice = 'A';
		}
		//play with myself
		while (1) {
			printf("Please enter your choice: ");
			scanf("%s", buffer);
			if (strcmp(buffer, "STAND") == 0)
				if (calculate(hand, handCnt) < 17)
					printf("You still need to play\n");
				else 
					break;
			else if (strcmp(buffer, "HIT") == 0) {
				printf("The card you get is: %d\n", card[cur]);
				hand[handCnt++] = card[cur++];
			}
			if (calculate(hand, handCnt) == -1) {
				printf("BUST\n");
				break;
			}
		}
		myCard = calculate(hand, handCnt);
		//compare the credits
		for(i = 0; i < k; ++i) {
			write(connfd[i], &myCard, sizeof(int));
			close(connfd[i]);
		}
	
	}
}
