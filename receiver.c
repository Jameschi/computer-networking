#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>

#define BUFSIZE 1100
#define PAYLOADSIZE 1000

void error(char *msg) {
	perror(msg);
}

int main(int argc, char *argv[]) {
	int sockfd, portno;
	int clientlen;
	struct sockaddr_in serveraddr;
	struct sockaddr_in clientaddr;
	char buf[BUFSIZE];
	int optval;
	int n;

	/* check command line arguments */
	if (argc != 3) {
		fprintf(stderr, "usage: %s <receiverport> <output filepath>\n", argv[0]);
		exit(1);
	}
	portno = atoi(argv[1]);

	/* socket: create the parent socket */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");

	/* Eliminates "ERROR on binding: Address already in use" error. */
	optval = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void *) &optval , sizeof(int));
	
	/* build the server's Internet address */
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(portno);

	/* bind: associate the parent socket with a port */
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
		error("ERROR on binding");
	
	/* open file for output flush */	
	FILE* output = fopen(argv[2], "w");

	/* main loop: wait for a datagram */
	clientlen = sizeof(clientaddr);
	int seq, curSeq = 0, dataSize;
	char type[10];
	char buffer[32][PAYLOADSIZE];
	int packetSize[32];
	bool check[32] = {0};
	int needFlush = 0;
	int i;
	
	while (1) {
		/* recvfrom: receive a UDP datagram from sender */
		bzero(buf, BUFSIZE);
		if ((n = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, &clientlen)) < 0)
			error("ERROR in recvfrom");
		sscanf(buf, "%d %d %s", &seq, &dataSize, type);
		if (seq == -1) { /* seq = -1 means fin */
			/* just flush all data available to file and send finack back to sender to indicate successful transmission */
			printf("recv\t%s\n", type);
			for (i = 0; i < 32; ++i) {
				if (check[i] == true) {
					fwrite(buffer[i], sizeof(char), packetSize[i], output);
				}
			}
			sprintf(buf, "%d %d %s", seq, -1, "finack");
			if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, clientlen)) < 0)
				error("ERROR in sendto");
			printf("send\tfinack\n");
			printf("flush\n");
			break;
		}
		else if (seq >= (curSeq + 1) * 32) { /* check if overflow */
			/* drop the overflowed packet, no need to send ack back to sender */
			printf("drop\t%s\t#%d\n", type, seq + 1);
			if (needFlush == 32) {/* if all packets are received, flush to the file */
				for (i = 0; i < 32; ++i) {
					fwrite(buffer[i], sizeof(char), packetSize[i], output);
					check[i] = false;
				}
				printf("flush\n");
				needFlush = 0;
				++curSeq;
			}
		}
		else if (seq < curSeq * 32) {/* check if downflow */
			printf("ignr\t%s\t#%d\n", type, seq + 1);
			/* send ack back to sender */
			sprintf(buf, "%d %d %s", seq, dataSize, "ack");
			if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, clientlen)) < 0)
				error("ERROR in sendto");
			printf("send\tack\t#%d\n", seq + 1);
		}
		else { /* packet received is in range, check if it is duplicated and needed to be ignored. */
			if (check[seq % 32] == false) {/* haven't received this packet */
				memcpy(buffer[seq % 32], buf + BUFSIZE - PAYLOADSIZE, PAYLOADSIZE);
				check[seq % 32] = true;
				packetSize[seq % 32] = dataSize;
				printf("recv\t%s\t#%d\n", type, seq + 1);
				++needFlush;
			}
			else {/* just ignore, since already received. But still need to resend ack */
				printf("ignr\t%s\t#%d\n", type, seq + 1);
			}
			/* sendto: send ack back to the sender */
			sprintf(buf, "%d %d %s", seq, dataSize, "ack");
			if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, clientlen)) < 0)
				error("ERROR in sendto");
			printf("send\tack\t#%d\n", seq + 1);
		}
	}
	fclose(output);
}
