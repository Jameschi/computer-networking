#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#define BUFSIZE 1100
#define PAYLOADSIZE 1000

void error(char *msg) {
	perror(msg);
}

bool drop(float droprate) {
	int a = 32000;
	while (a >= 30000)
		a = rand();
	a = a % 10000 + 1;
	int bound = droprate * 10000;
	if (a <= bound) {
		return true;

	}
	else
		return false;
}

int main(int argc, char *argv[]) {
	int sockfd, serverportno, senderportno, receiverportno, n;
	unsigned int serverlen, clientlen, senderlen, receiverlen;
	struct sockaddr_in senderaddr;
	struct sockaddr_in receiveraddr;
	struct sockaddr_in serveraddr;
	struct sockaddr_in clientaddr;
	struct hostent *sender;
	struct hostent *receiver;
	char *sendername;
	char *receivername;
	char buf[BUFSIZE];
	float droprate;
	
	/* check command line arguments */
	if (argc != 7) {
		fprintf(stderr,"usage: %s <sendername> <senderport> <receivername> <receiverport> <agentport> <lossrate>\n", argv[0]);
		exit(1);
	}
	
	sendername = argv[1];
	senderportno = atoi(argv[2]);
	senderlen = sizeof(senderaddr);
	receivername = argv[3];
	receiverportno = atoi(argv[4]);
	receiverlen = sizeof(receiveraddr);
	serverportno = atoi(argv[5]);
	droprate = atof(argv[6]);
	clientlen = sizeof(clientaddr);
	
	/* socket: create UDP socket */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) 
        error("ERROR opening socket");
	
	/* build the server's Internet address */
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(serverportno);
	
	/* bind: asscociate the parent socket with a port */
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
		error("ERROR on binding");
	
	/* build the sender's Internet address */
	sender = gethostbyname(sendername);
	bzero((char *) &senderaddr, senderlen);
	senderaddr.sin_family = AF_INET;
	bcopy((char *)sender->h_addr, (char *) &senderaddr.sin_addr.s_addr, sender->h_length);
	senderaddr.sin_port = htons(senderportno);

	/* build the receiver's Internet address */
	receiver = gethostbyname(receivername);
	bzero((char *) &receiveraddr, receiverlen);
	receiveraddr.sin_family = AF_INET;
	bcopy((char *)receiver->h_addr, (char *) &receiveraddr.sin_addr.s_addr, receiver->h_length);
	receiveraddr.sin_port = htons(receiverportno);
	
	int seq, dataSize;
	char type[10];
	float dropnum = 0.f, totalnum = 0.f;
	srand(time(NULL));
	
	while (1) { /* deal with get and forward */
		bzero(buf, BUFSIZE);
		if ((n = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, &clientlen)) < 0)
			error("ERROR in recvfrom");
		sscanf(buf, "%d %d %s", &seq, &dataSize, type);
		if (strcmp(type, "data") == 0) {
			totalnum += 1.f;
			printf("get\t%s\t#%d\n", type, seq + 1);
			/* sendto receiver */
			if (drop(droprate) == false){
				if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &receiveraddr, receiverlen)) < 0)
					error("ERROR in sendto");
				printf("fwd\t%s\t#%d,\t", type, seq + 1);
				printf("loss rate = %f\n", dropnum / totalnum);
			}
			else {
				dropnum += 1.f;
				printf("drop\t%s\t#%d,\t", type, seq + 1);
				printf("loss rate = %f\n", dropnum / totalnum);
			}

		}
		else if (strcmp(type, "fin") == 0) {
			totalnum += 1.f;
			printf("get\t%s\n", type);
			/* sendto receiver */
			if (drop(droprate) == false){
				if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &receiveraddr, receiverlen)) < 0)
					error("ERROR in sendto");
				printf("fwd\t%s\n", type);
			}
			else {
				dropnum += 1.f;
				printf("drop\t%s\t", type);
				printf("loss rate = %f\n", dropnum / totalnum);
			}
			
		}
		else if (strcmp(type, "ack") == 0) {
			printf("get\t%s\t#%d\n", type, seq + 1);
			/* sendto sender */
			if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &senderaddr, senderlen)) < 0)
				error("ERROR in sendto");
			else {
				printf("fwd\t%s\t#%d\n", type, seq + 1);
			}
		}
		else if (strcmp(type, "finack") == 0) {
			printf("get\t%s\n", type);
			/* sendto sender */
			if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &senderaddr, senderlen)) < 0)
				error("ERROR in sendto");
			else {
				printf("fwd\t%s\n", type);
			}
			break;
		}
	}
	return 0;
}
