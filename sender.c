#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#define BUFSIZE 1100
#define PAYLOADSIZE 1000

void error(char *msg) {
	perror(msg);
}

int max(int a, int b) {
	return (a > b) ? a : b;
}

size_t get_filesize(const char* filename) {
	struct stat st;
	stat(filename, &st);
	return st.st_size;
}

int main(int argc, char *argv[]) {
	int sockfd, agentportno, serverportno, n;
	int agentlen, clientlen;
	struct sockaddr_in agentaddr;
	struct sockaddr_in serveraddr;
	struct sockaddr_in clientaddr;
	struct hostent *agent;
	char *hostname;
	char buf[BUFSIZE];
	clientlen = sizeof(clientaddr);
	
	/* check command line arguments */
	if (argc != 5) {
		fprintf(stderr,"usage: %s <agentname> <agentport> <senderport> <inputfilepath>\n", argv[0]);
		exit(1);
	}
	
	hostname = argv[1];
	agentportno = atoi(argv[2]);
	agentlen = sizeof(agentaddr);
	serverportno = atoi(argv[3]);
	
	/* socket: create UDP socket */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) 
        error("ERROR opening socket");
	struct timeval timeout = {1, 0};
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(timeout));

	/* gethostbyname: get the agent's DNS entry */
	agent = gethostbyname(hostname);
	if (agent == NULL) {
		fprintf(stderr,"ERROR, no such host as %s\n", hostname);
		exit(1);
	}

	/* build the agent's Internet address */
	bzero((char *) &agentaddr, agentlen);
	agentaddr.sin_family = AF_INET;
	bcopy((char *)agent->h_addr, (char *) &agentaddr.sin_addr.s_addr, agent->h_length);
	agentaddr.sin_port = htons(agentportno);
	
	/* build the server's Internet address */
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(serverportno);

	/* bind: associate the parent socket with a port */
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
		error("ERROR on binding");
	
	/* get a message from the file in argv[3] */
	int file_fd;
	if ((file_fd = open(argv[4], O_RDWR)) < 0) {
		error("failed to open input file");
	}
	int file_size;
	if ((file_size = get_filesize(argv[4])) < 0) {
		error("failed to get filesize");
	}
	char* src = mmap(NULL, file_size, PROT_READ, MAP_PRIVATE, file_fd, 0);
	
	int dataNum = (file_size % PAYLOADSIZE == 0) ? file_size / PAYLOADSIZE : file_size / PAYLOADSIZE + 1;
	char data[dataNum][PAYLOADSIZE];
	int i = 0;
	int offset;
	for(offset = 0; offset < file_size; offset += PAYLOADSIZE)
		memcpy(data[i++], src + offset, (offset + PAYLOADSIZE < file_size) ? PAYLOADSIZE : file_size - offset);
	
	int window = 1, threshold = 16, seq;
	int* ackArr;
	bool success;
	int maxSendSeq = -1;
	int maxUnackedSeq = 0;
	
	while (maxUnackedSeq < dataNum) {
		ackArr = (int*)malloc(window * sizeof(int));
		/* send the message to the receiver */
		/* packet : seq number + payload size + type of packet + payload */
		for (i = 0; i < window; ++i) {
			if (i + maxUnackedSeq >= dataNum)
				break;
			bzero(buf, BUFSIZE);
			sprintf(buf, "%d %d %s", i + maxUnackedSeq, (i + maxUnackedSeq != dataNum - 1) ? PAYLOADSIZE : file_size - PAYLOADSIZE * (i + maxUnackedSeq), "data");
			memcpy(buf + BUFSIZE - PAYLOADSIZE, data[i + maxUnackedSeq], PAYLOADSIZE);
			if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &agentaddr, agentlen)) < 0)
				error("ERROR in sendto");
			else {
				if (maxSendSeq < i + maxUnackedSeq) {
					maxSendSeq = i + maxUnackedSeq;
					printf("send\tdata\t#%d,\twinSize = %d\n", i + maxUnackedSeq + 1, window);
				}
				else {
					printf("resnd\tdata\t#%d,\twinSize = %d\n", i + maxUnackedSeq + 1, window);
				}
			}
		}
		/* print the receiver's reply */
		for (i = 0; i < window; ++i) {
			if (i + maxUnackedSeq >= dataNum)
				break;
			/* handle timeout and retransmit */
			bzero(buf, BUFSIZE);
			if ((n = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, &clientlen)) < 0) {
				ackArr[i] = -1;
				threshold = max(window / 2, 1);
				printf("time\tout,\tthreshold = %d\n", threshold);
			}
			else {
				printf("recv\tack\t#%d\n", atoi(buf) + 1);
				ackArr[i] = atoi(buf);
			}
		}
		int checkAck = i;
		int j;
		/* check if all the ack received match the data we sent */
		/* if not, retransmit */
		success = true;
		for (i = 0; i < window; ++i) {
			if (i + maxUnackedSeq >= dataNum)
				break;
			for (j = 0; j < checkAck; ++j) {
				if (ackArr[j] == maxUnackedSeq + i)
					break;
			}
			if (j == checkAck) {
				success = false;
				window = 1;
				maxUnackedSeq += i;
				break;
			}
		}
		if (success) {
			maxUnackedSeq += window;
			(window < threshold) ? window *= 2 : ++window;
		}
		if (ackArr)
			free(ackArr);
	}
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, NULL, sizeof(timeout));
	while (1) {
		/* send fin at here */
		bzero(buf, BUFSIZE);
		sprintf(buf, "%d %d %s", -1, -1, "fin");
		if ((n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &agentaddr, agentlen)) < 0)
			error("ERROR in fin sendto");
		else {
			printf("send\tfin\n");
		}
		/* confirm finack */
		bzero(buf, BUFSIZE);
		if ((n = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &agentaddr, &agentlen)) < 0) {
			//error("ERROR in finack recvfrom");
			printf("timeout\tfinack\n");
		}
		else {
			printf("recv\tfinack\n");
			break;
		}
	}
	return 0;
}
