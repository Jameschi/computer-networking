#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "ai.h"
int hand[4][21];
int cnt[4] = {2, 2, 2, 2};
int calculate(int* x, int cnt) {
	int i, result = 0, ace = 0;
	for (i = 0; i < cnt; ++i) {
		if (x[i] == 1) {
			++ace;
			result += 11;
		}
		else if (x[i] >= 10)
			result += 10;
		else result += x[i];
	}
	while (ace > 0 && result > 21) {
		--ace;
		result -= 10;
	}
	if (result > 21)
		return -1;
	else
		return result;
}
int main(int argc, char *argv[])
{
	char buffer[50];
	int sockfd;
	char command;
	struct sockaddr_in serverAddress;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	bzero(&serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(5000);
	inet_pton(AF_INET, argv[1], &serverAddress.sin_addr);
	connect(sockfd, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
	write(sockfd, &argv[2][0], sizeof(char));//tell server game type, determined by the first player
	read(sockfd, &command, sizeof(char));//to confirm game type from server
	if (command == 'G' || command == 'g') {//guess number
		int i, num[4], result, bet;
		printf("Please enter your bet: ");
		scanf("%d", &bet);
		prepare();
		for(i = 0; i < 10 && isOver != 1; ++i) {
			ask(num);
			printf("Guess number is: %d%d%d%d\n", num[0], num[1], num[2], num[3]);
			int j;
			for(j = 0; j < 4; ++j) {
				write(sockfd, &num[j], sizeof(int));
			}
			read(sockfd, &a, sizeof(int));
			read(sockfd, &b, sizeof(int));
			printf("%dA%dB\n", a, b);
			think();
		}
		int min;
		read(sockfd, &min, sizeof(int));
		read(sockfd, &result, sizeof(int));
		printf("The result is: %d\n", result);
		printf("The min number of guess among all players is: %d\n", min);
		if (result == min)
			printf("You win %d dollars.\n", bet);
		else 
			printf("You lose %d dollars.\n", bet);
	}
	else if (command = 'B' || command == 'b') {//blackjack part
		int split = 1, bet;
		int card[8];
		int i, allCards;
		printf("Please enter your bet: ");
		scanf("%d", &bet);
		read(sockfd, &card[0], sizeof(int));//read first card
		read(sockfd, &card[1], sizeof(int));//read second card
		printf("Your current two cards are: %d %d\n", card[0], card[1]);
		if (calculate(card, 2) != 21) {//blackjack
			for (i = 0; i < 13; ++i) {
				read(sockfd, &allCards, sizeof(int));
				printf("#Visible %d until now: %d\n", i + 1, allCards);
			}
			printf("Do you want to double your bet? y/n ");
			scanf("%s", buffer);
			if (strcmp(buffer, "y") == 0)
				bet *= 2;
			if (card[0] == card[1]) {
				printf("SPLIT? y/n ");
				scanf("%s", buffer);
				if (strcmp("y", buffer) == 0) {//first split
					++split;//split = 2
					write(sockfd, "P", 1);
					read(sockfd, &card[2], sizeof(int));//read the card dealt by server
					read(sockfd, &card[3], sizeof(int));//read the card dealt by server
					printf("cards in split#1 are: %d %d\n", card[0], card[2]);
					int secondSplit = 0;
					if (card[0] == card[2]) {
						printf("SPLIT? y/n ");
						scanf("%s", buffer);
						if (strcmp("y", buffer) == 0) {//second split
							++split;//split = 3
							secondSplit = 1;
							write(sockfd, "P", 1);
							read(sockfd, &card[4], sizeof(int));//read the card dealt by server
							read(sockfd, &card[5], sizeof(int));//read the card dealt by server
							hand[0][0] = card[0];
							hand[0][1] = card[4];
							hand[1][0] = card[2];
							hand[1][1] = card[5];
							printf("cards in split# 1 are: %d %d\n", card[0], card[4]);
							printf("cards in split# 2 are: %d %d\n", card[2], card[5]);
						}
						else {
							hand[0][0] = card[0];
							hand[0][1] = card[2];
						}
					}
					else {
						hand[0][0] = card[0];
						hand[0][1] = card[2];
					}
					printf("cards in split#%d are: %d %d\n", split, card[1], card[3]);
					if (card[1] == card[3]) {
						printf("SPLIT? y/n  ");
						scanf("%s", buffer);
						if (strcmp("y", buffer) == 0) {//third split
							++split;
							write(sockfd, "P", 1);
							read(sockfd, &card[6], sizeof(int));//read the card dealt by server
							read(sockfd, &card[7], sizeof(int));//read the card dealt by server
							hand[1 + secondSplit][0] = card[1];
							hand[1 + secondSplit][1] = card[6];
							hand[2 + secondSplit][0] = card[3];
							hand[2 + secondSplit][1] = card[7];
							printf("cards in split#%d are: %d %d\n", split - 1, card[1], card[6]);
							printf("cards in split#%d are: %d %d\n", split, card[3], card[7]);
						}
						else {
							hand[1 + secondSplit][0] = card[1];
							hand[1 + secondSplit][1] = card[3];
						}
					}
					else {
						hand[1 + secondSplit][0] = card[1];
						hand[1 + secondSplit][1] = card[3];
					}
				}
				else {
					hand[0][0] = card[0];
					hand[0][1] = card[1];
				}
					
			}
			else {
				hand[0][0] = card[0];
				hand[0][1] = card[1];
			}
			
			for(i = 0; i < split; ++i) {
				int hitCard;
				while(1) {
					printf("Please enter your option for split# %d: ", i + 1);
					scanf("%s", buffer);
					if (strcmp(buffer, "HIT") == 0) {
						write(sockfd, "I", 1);
						read(sockfd, &hitCard, sizeof(int));
						printf("The card you get is %d\n", hitCard);
						hand[i][cnt[i]++] = hitCard;
						if (calculate(hand[i], cnt[i]) == -1) {
							printf("BUST\n");
							break;
						}
					}
					else if (strcmp(buffer, "STAND") == 0) {
						break;
					}
				}
			}
		}//end blackjack if
		else {
			printf("Blackjack!!!\n");
			hand[0][0] = 1;
			hand[0][1] = 10;
			cnt[0] = 2;
			bet *= 1.5;
		}
		write(sockfd, "T", 1);
		int dealer;
		read(sockfd, &dealer, sizeof(int));
		printf("dealer is %d\n", dealer);
		for (i = 0; i < split; ++i) {
			if (calculate(hand[i], cnt[i]) == -1)//self BUST
				printf("split# %d lose %d dollars.\n", i + 1, bet);
			else if (dealer == -1 || dealer < calculate(hand[i], cnt[i]))
				printf("split# %d win %d dollars\n", i + 1, bet);
			else if (dealer == calculate(hand[i], cnt[i]))
				printf("split# %d draw, bet taken back.\n", i + 1);
			else
				printf("split# %d lose %d dollars.\n", i + 1, bet);
		}
	//If receive R, rejected by server
	}
	close(sockfd);
}
